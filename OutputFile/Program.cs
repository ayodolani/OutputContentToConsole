﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutputFile
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] fruits = {"Banana", "mango", "apple"};
            String path = @"C:\Users\USER\Desktop\fruits.txt";
            File.WriteAllLines(path, fruits);
            String reader = File.ReadAllText(path);
            Console.WriteLine(reader);
            Console.ReadKey(true);
        }
    }
}
